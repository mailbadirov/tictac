import './scss/app.scss';

let chBoxArr = [];
let xPlayed = true;
let playedCount = 0;

let myCreateElement = (tagName, parentName, className = '', inclText = '') => {
  const tempEl = document.createElement(tagName);
  if (className != '') tempEl.classList.add(className);
  parentName.appendChild(tempEl);
  if (inclText != '') tempEl.innerHTML = inclText;

  return tempEl;
};

const colRows = myCreateElement('div', document.body, 'colRows');
const stateStr = myCreateElement('p', document.body, 'stateStr', 'State:');

document.write('<span>Количество ячеек: </span>');
const colInput = myCreateElement('INPUT', document.body, '', 'Hello World!');
colInput.setAttribute('type', 'number');
colInput.setAttribute('value', '3');

function createField(colNum) {
  colRows.style.width = colNum * 64 + 'px';

  document.querySelectorAll('.chBox').forEach((elem) => elem.remove());
  xPlayed = true;
  playedCount = 0;

  if (colNum > 1 && colNum < 200)
    for (let i = 0; i < colNum ** 2; i++) {
      chBoxArr[i] = myCreateElement('div', colRows, 'chBox');
    }
}

colInput.addEventListener('input', function () {
  createField(this.value);
});

function whoWin(curPlayer) {
  let count = +colInput.value;

  for (let i = 0; i < count; i++) {
    let winRow = true,
      winColumn = true,
      winLeftTop = true,
      winLeftBottom = true;

    for (let k = 0; k < count; k++) {
      if (!chBoxArr[i * count + k].classList.contains(curPlayer))
        winRow = false;
      if (!chBoxArr[k * count + i].classList.contains(curPlayer))
        winColumn = false;
      if (!chBoxArr[k * (count + 1)].classList.contains(curPlayer))
        winLeftTop = false;
      if (!chBoxArr[count - 1 + k * (count - 1)].classList.contains(curPlayer))
        winLeftBottom = false;
    }

    if (winRow || winColumn || winLeftTop || winLeftBottom) {
      alert('победа ' + curPlayer.toUpperCase());
      createField(count);
      break;
    } else if (playedCount == count ** 2) {
      alert('ничья');
      createField(count);
      break;
    }
  }
}

document.addEventListener('click', (event) => {
  const targetObj = event.target;
  const isBox = targetObj.classList.contains('chBox');
  const isPlayed = targetObj.classList.contains('played');

  if (isBox && !isPlayed) {
    targetObj.classList.add('played');
    targetObj.classList.add(xPlayed ? 'x' : 'o');

    xPlayed = !xPlayed;
    playedCount += 1;
    whoWin(xPlayed ? 'o' : 'x');
  }
});

createField(colInput.value);
